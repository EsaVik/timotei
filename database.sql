CREATE TABLE Players
	(
	PlayerID    INTEGER    PRIMARY KEY    NOT NULL,
	Name        VARCHAR(20)   NOT NULL    UNIQUE
	);

CREATE TABLE Statistics 
	(
	StatisticsID      INTEGER    PRIMARY KEY    NOT NULL,
	PlayerID          INTEGER    NOT NULL,
	Money             INTEGER    NOT NULL,
        MoneyEarned       INTEGER    NOT NULL       DEFAULT 0,
        MoneySpent        INTEGER    NOT NULL       DEFAULT 0,
        SmartPostsBought  INTEGER    NOT NULL       DEFAULT 0,
	SmartPostsSold    INTEGER    NOT NULL       DEFAULT 0,

	FOREIGN KEY(PlayerID) REFERENCES Players(PlayerID)
	);

CREATE TABLE PostalCodes
	(
	PostalCodeID      INTEGER       PRIMARY KEY    NOT NULL,
	PostalCode        VARCHAR(5)    NOT NULL,
	City              VARCHAR(20)
	);

CREATE TABLE Locations
	(
	LocationID      INTEGER       PRIMARY KEY    NOT NULL,
	PostalCodeID    INTEGER       NOT NULL,
	Address         VARCHAR(30)   NOT NULL,
	Latitude	DOUBLE        NOT NULL,
	Longitude       DOUBLE        NOT NULL,

	FOREIGN KEY(PostalCodeID) REFERENCES PostalCodes(PostalCodeID)
	);

CREATE TABLE SmartPosts
	(
	SmartPostID    INTEGER       PRIMARY KEY    NOT NULL,
	LocationID     INTEGER       NOT NULL,
	Name           VARCHAR(50)   NOT NULL,
	Availability   VARCHAR(50)   NOT NULL,

	FOREIGN KEY(LocationID) REFERENCES Locations(LocationID)
	);

CREATE TABLE BoughtSmartPosts
	(
	PlayerID       INTEGER    NOT NULL,
	SmartPostID    INTEGER    NOT NULL,

	PRIMARY KEY(PlayerID, SmartPostID),

	FOREIGN KEY(PlayerID) REFERENCES Players(PlayerID),
	FOREIGN KEY(SmartPostID) REFERENCES SmartPosts(SmartPostID)
	);

CREATE TABLE Objects
	(
	ObjectID    INTEGER       PRIMARY KEY    NOT NULL,
	Name        VARCHAR(30)   NOT NULL
	);

CREATE TABLE Classes
	(
	ClassID        INTEGER    PRIMARY KEY    NOT NULL,
	Price          INTEGER    NOT NULL,
	SizeLimit      INTEGER    NOT NULL,
	DistanceLimit  INTEGER    NOT NULL
	);

CREATE TABLE Orders
	(
	OrderID          INTEGER    PRIMARY KEY    NOT NULL,
	PlayerID         INTEGER    NOT NULL,
	SourceID         INTEGER    NOT NULL,
	DestinationID    INTEGER    NOT NULL,
	ObjectID         INTEGER    NOT NULL,
	Delivered        BOOLEAN    NOT NULL,

	FOREIGN KEY(PlayerID) REFERENCES Players(PlayerID),
	FOREIGN KEY(SourceID) REFERENCES SmartPosts(SmartPostID),
	FOREIGN KEY(DestinationID) REFERENCES SmartPosts(SmartPostID),
	FOREIGN KEY(ObjectID) REFERENCES Objects(ObjectID)
	);

CREATE TABLE Parcels
	(
	ParcelID          INTEGER      PRIMARY KEY    NOT NULL,
	PlayerID          INTEGER      NOT NULL,
	SourceID          INTEGER      NOT NULL,
        DestinationID     INTEGER      NOT NULL,
        ObjectID          INTEGER      NOT NULL,
        ClassID           INTEGER      NOT NULL,
	Name              VARCHAR(20)  NOT NULL,
	Sent              BOOLEAN      NOT NULL,

	FOREIGN KEY(PlayerID) REFERENCES Players(PlayerID),
	FOREIGN KEY(SourceID) REFERENCES SmartPosts(SmartPostID),
	FOREIGN KEY(DestinationID) REFERENCES SmartPosts(SmartPostID),
	FOREIGN KEY(ObjectID) REFERENCES Objects(ObjectID),
	FOREIGN KEY(ClassID) REFERENCES Classes(ClassID)
	);

CREATE TABLE Logs
	(
	LogID        INTEGER      PRIMARY KEY    NOT NULL,
	ParcelID     INTEGER      NOT NULL,
	Timestamp    DATETIME     DEFAULT CURRENT_TIMESTAMP,

	FOREIGN KEY(ParcelID) REFERENCES Parcels(ParcelID)
	);

CREATE VIEW LogData AS 
	SELECT Parcels.PlayerID, Parcels.SourceID, Parcels.DestinationID, Objects.Name, Logs.Timestamp
	FROM (Parcels INNER JOIN Logs ON Parcels.ParcelID = Logs.ParcelID)
	INNER JOIN Objects ON Parcels.ObjectID = Objects.ObjectID;