package timotei;

import java.util.LinkedHashMap;
import java.util.ArrayList;

/*A class for holding Parcels*/
public class Storage {

    private LinkedHashMap<String, Parcel> parcels;
    private static Storage instance;

    private Storage() {
    }

    public static Storage getInstance() {
        if (instance == null) {
            instance = new Storage();
            instance.parcels = new LinkedHashMap();
        }
        return instance;
    }

    /*Takes in objectID, returns ShippingObject of corresponding type*/
    public ShippingObject objectSelection(int type) {
        switch (type) {
            case 1:
                return new Tiili();
            case 2:
                return new Kukkaruukku();
            case 3:
                return new Jalkapallo();
            case 4:
                return new Palapeli();
        }
        return null;
    }

    public ArrayList getParcels() {
        ArrayList keys = new ArrayList();
        for (String key : parcels.keySet()) {
            keys.add(key);
        }
        return keys;
    }

    public Parcel getParcel(String name) {
        return parcels.get(name);
    }

    public void addParcel(int classID, String name, String source, String destination, ShippingObject object, Double distance, int sizeLimit, int distanceLimit, int price) throws SizeException, DistanceException {
        switch (classID) {
            case 1:
                parcels.put(name, new FirstClassParcel(name, object, source, destination, classID, distance, sizeLimit, distanceLimit, price));
                break;
            case 2:
                parcels.put(name, new SecondClassParcel(name, object, source, destination, classID, distance, sizeLimit, distanceLimit, price));
                break;
            case 3:
                parcels.put(name, new ThirdClassParcel(name, object, source, destination, classID, distance, sizeLimit, distanceLimit, price));
                break;
        }
    }

    public void removeParcel(String name) {
        parcels.remove(name);
    }

}
