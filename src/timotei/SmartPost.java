package timotei;

import java.util.ArrayList;

/*A class for holding SmartPost data*/
public class SmartPost {
    
    private GeoPoint geoPoint;
    private String name;
    private String info;
    private String postalCode;
    private String city;
    private int ID;
    
    public SmartPost(String name, String availability, String address, Double latitude, Double longitude, String postalCode, String city, String ID) {
        geoPoint = new GeoPoint(latitude, longitude);
        this.name = name;
        this.postalCode = postalCode;
        this.city = city;
        info = name + ", " + availability;
        this.ID = Integer.parseInt(ID);
    }

    public String getName() {
        return name;
    }

    public String getInfo() {
        return info;
    }

    public ArrayList getGeoData() {
        return geoPoint.geoData;
    }
    
    public int getID() {
        return ID;
    }
    
}
