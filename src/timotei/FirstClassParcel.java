package timotei;

/*A type of Parcel*/
public class FirstClassParcel extends Parcel {

    public FirstClassParcel(String nameInput, ShippingObject objectInput, String sourceInput, String destinationInput, int classID, double distance, int sizeLimit, int distanceLimit, int priceInput) throws SizeException, DistanceException {
        super(nameInput, objectInput, sourceInput, destinationInput, classID, distance, sizeLimit, distanceLimit, priceInput);
        if (objectInput.getSize() > sizeLimit) {
            throw new SizeException();
        }
        if (distance > distanceLimit) {
            throw new DistanceException();
        }
    }

    @Override
    public int send(){
        shippingObject.makeBroken();
        return price;
    }

}
