package timotei;

import java.io.Serializable;

/*Abstract ShippingObject class for parcels*/
public abstract class ShippingObject implements Serializable {

    protected boolean breakable;
    protected boolean broken;
    protected int size;

    public int getSize() {
        return size;
    }

    public boolean isBroken() {
        return broken;
    }

    public void makeBroken() {
        if (breakable) {
            broken = true;
        }
    }

    public String getName() {
        return this.getClass().getSimpleName();
    }

}
