package timotei;

/*A type of ShippingObject*/
public class Kukkaruukku extends ShippingObject {
    
    public Kukkaruukku() {
        breakable = true;
        broken = false;
        size = 2000;
    }
    
}
