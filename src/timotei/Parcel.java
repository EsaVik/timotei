package timotei;

/*Abstract Parcel class*/
public abstract class Parcel {
    
    protected String name;
    protected ShippingObject shippingObject;
    protected String source;
    protected String destination;
    protected int sizeLimit;
    protected int price;
    protected int parcelClass;
    protected int distanceLimit;

    public Parcel(String nameInput, ShippingObject objectInput, String sourceInput, String destinationInput, int classID, double distance, int sizeLimit, int distanceLimit, int priceInput) throws SizeException, DistanceException {
        name = nameInput;
        shippingObject = objectInput;
        source = sourceInput;
        destination = destinationInput;
        this.sizeLimit = sizeLimit;
        this.distanceLimit = distanceLimit;
        price = priceInput;
        parcelClass = classID;
    }

    public String getName() {
        return name;
    }

    public ShippingObject getObject() {
        return shippingObject;
    }
    
    public int getPrice() {
        return price;
    }

    public int getParcelClass() {
        return parcelClass;
    }
    
    public int send() {
        return price;
    }
    
    public String getSource() {
        return source;
    }

    public String getDestination() {
        return destination;
    }
}
