package timotei;

import java.util.ArrayList;

/*Holds SmartPost's geodata*/
public class GeoPoint {
    
    ArrayList geoData;

    public GeoPoint(Double latitude, Double longitude) {
        geoData = new ArrayList();
        geoData.add(latitude);
        geoData.add(longitude);
    }
    
    
}
