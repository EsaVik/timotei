package timotei;

/*An exception to be thrown when there is not sufficient money for the operation*/
public class MoneyException extends Exception {
}
