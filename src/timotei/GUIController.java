package timotei;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Worker;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

/*Controller for the main GUI screen*/
public class GUIController implements Initializable {

    @FXML
    private WebView mapView;
    @FXML
    private ComboBox<?> buyComboBox;
    @FXML
    private ComboBox<?> sellComboBox;
    @FXML
    private Label moneyLabel;
    @FXML
    private ComboBox<?> sendParcelComboBox;
    @FXML
    private ComboBox<?> createParcelComboBox;
    @FXML
    private TextField nameTextField;
    @FXML
    private ComboBox<?> objectComboBox;
    @FXML
    private ComboBox<?> classComboBox;
    @FXML
    private ComboBox<?> sourceComboBox;
    @FXML
    private ComboBox<?> destinationComboBox;
    @FXML
    private ListView<?> ordersListView;
    @FXML
    private ListView<?> logListView;
    @FXML
    private TextArea statsTextArea;
    private static GUIController gc;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        gc = this;
        mapView.getEngine().load(getClass().getResource("index.html").toExternalForm());
        addClasses();
        updateStatistics();
        updateComboBoxes();
        updateParcelComboBoxes();
        initialDrawBoughtSmartPosts();
    }

    @FXML
    private void buyButtonPressed(ActionEvent event) {
        if (buyComboBox.getSelectionModel().getSelectedItem() != null) {
            String name = buyComboBox.getSelectionModel().getSelectedItem().toString();
            GameState gs = GameState.getInstance();

            try {
                gs.buySmartPost(name);
            } catch (MoneyException ex) {
                try {
                    Stage stage = new Stage();
                    Parent root = FXMLLoader.load(getClass().getResource("MoneyExceptionScreen.fxml"));
                    Scene scene = new Scene(root);
                    stage.setScene(scene);
                    stage.show();
                } catch (IOException ex1) {
                    Logger.getLogger(GUIController.class.getName()).log(Level.SEVERE, null, ex1);
                }
            }

            mapView.getEngine().executeScript("document.goToLocation(" + gs.getSmartPost(name).getGeoData() + ",'" + gs.getSmartPost(name).getInfo() + "','blue')");
            updateStatistics();
            updateComboBoxes();
        }
    }

    @FXML
    private void sellButtonPressed(ActionEvent event) {
        if (sellComboBox.getSelectionModel().getSelectedItem() != null) {
            String name = sellComboBox.getSelectionModel().getSelectedItem().toString();
            GameState gs = GameState.getInstance();
            mapView.getEngine().executeScript("document.deleteMarker(" + sellComboBox.getSelectionModel().getSelectedIndex() + ")");

            gs.sellSmartPost(name);

            updateStatistics();
            updateComboBoxes();
        }
    }

    @FXML
    private void sendButtonPressed(ActionEvent event) {
        if (sendParcelComboBox.getSelectionModel().getSelectedItem() != null) {
            String name = sendParcelComboBox.getSelectionModel().getSelectedItem().toString();
            GameState gs = GameState.getInstance();

            try {
                ArrayList pathArray = new ArrayList();
                pathArray.addAll(gs.getSmartPost(gs.getParcel(name).getSource()).getGeoData());
                pathArray.addAll(gs.getSmartPost(gs.getParcel(name).getDestination()).getGeoData());
                int parcelClass = gs.getParcel(name).getParcelClass();
                gs.sendParcel(name);
                mapView.getEngine().executeScript("document.createPath(" + pathArray + ",'red'," + parcelClass + ")");
            } catch (MoneyException ex) {
                try {
                    Stage stage = new Stage();
                    Parent root = FXMLLoader.load(getClass().getResource("MoneyExceptionScreen.fxml"));
                    Scene scene = new Scene(root);
                    stage.setScene(scene);
                    stage.show();
                } catch (IOException ex1) {
                    Logger.getLogger(GUIController.class.getName()).log(Level.SEVERE, null, ex1);
                }
            }

            updateStatistics();
            updateParcelComboBoxes();
            updateOrders();
            updateLog();
        }
    }

    @FXML
    private void deletePathsButtonPressed(ActionEvent event) {
        mapView.getEngine().executeScript("document.deletePaths()");
    }

    @FXML
    private void classInfoButtonPressed(ActionEvent event) {
        try {
            Stage stage = new Stage();
            Parent root = FXMLLoader.load(getClass().getResource("ClassInfo.fxml"));
            Scene scene = new Scene(root);
            stage.setScene(scene);
            stage.show();
        } catch (IOException ex) {
            Logger.getLogger(GUIController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    private void saveParcelButtonPressed(ActionEvent event) {
        GameState gs = GameState.getInstance();
        if (gs.getBoughtSmartPosts().size() > 0) {
            if (!(sourceComboBox.getSelectionModel().getSelectedItem().toString().equals(destinationComboBox.getSelectionModel().getSelectedItem().toString()))) {
                if (createParcelComboBox.getSelectionModel().getSelectedItem().toString().equals("Luo uusi paketti")) {
                    try {
                        String name = nameTextField.getText().trim();
                        if (name.isEmpty()) {
                            name = "Parcel";
                        }
                        String object = objectComboBox.getSelectionModel().getSelectedItem().toString();
                        String source = sourceComboBox.getSelectionModel().getSelectedItem().toString();
                        String destination = destinationComboBox.getSelectionModel().getSelectedItem().toString();
                        int classID = Integer.parseInt(classComboBox.getSelectionModel().getSelectedItem().toString());
                        Double distance = getDistance(source, destination);
                        gs.addParcel(name, object, source, destination, classID, distance);
                    } catch (ParcelNameUsedException ex) {
                        try {
                            Stage stage = new Stage();
                            Parent root = FXMLLoader.load(getClass().getResource("ParcelNameUsedExceptionScreen.fxml"));
                            Scene scene = new Scene(root);
                            stage.setScene(scene);
                            stage.show();
                        } catch (IOException ex1) {
                            Logger.getLogger(GUIController.class.getName()).log(Level.SEVERE, null, ex1);
                        }
                    } catch (SizeException ex) {
                        try {
                            Stage stage = new Stage();
                            Parent root = FXMLLoader.load(getClass().getResource("SizeExceptionScreen.fxml"));
                            Scene scene = new Scene(root);
                            stage.setScene(scene);
                            stage.show();
                        } catch (IOException ex1) {
                            Logger.getLogger(GUIController.class.getName()).log(Level.SEVERE, null, ex1);
                        }
                    } catch (DistanceException ex) {
                        try {
                            Stage stage = new Stage();
                            Parent root = FXMLLoader.load(getClass().getResource("DistanceExceptionScreen.fxml"));
                            Scene scene = new Scene(root);
                            stage.setScene(scene);
                            stage.show();
                        } catch (IOException ex1) {
                            Logger.getLogger(GUIController.class.getName()).log(Level.SEVERE, null, ex1);
                        }
                    }
                } else {
                    try {
                        String name = createParcelComboBox.getSelectionModel().getSelectedItem().toString();
                        String newName = nameTextField.getText().trim();
                        if (newName.isEmpty()) {
                            newName = "Parcel";
                        }
                        String object = objectComboBox.getSelectionModel().getSelectedItem().toString();
                        String source = sourceComboBox.getSelectionModel().getSelectedItem().toString();
                        String destination = destinationComboBox.getSelectionModel().getSelectedItem().toString();
                        int classID = Integer.parseInt(classComboBox.getSelectionModel().getSelectedItem().toString());
                        Double distance = getDistance(source, destination);
                        gs.updateParcel(name, newName, object, source, destination, classID, distance);
                    } catch (ParcelNameUsedException ex) {
                        try {
                            Stage stage = new Stage();
                            Parent root = FXMLLoader.load(getClass().getResource("ParcelNameUsedExceptionScreen.fxml"));
                            Scene scene = new Scene(root);
                            stage.setScene(scene);
                            stage.show();
                        } catch (IOException ex1) {
                            Logger.getLogger(GUIController.class.getName()).log(Level.SEVERE, null, ex1);
                        }
                    } catch (SizeException ex) {
                        try {
                            Stage stage = new Stage();
                            Parent root = FXMLLoader.load(getClass().getResource("SizeExceptionScreen.fxml"));
                            Scene scene = new Scene(root);
                            stage.setScene(scene);
                            stage.show();
                        } catch (IOException ex1) {
                            Logger.getLogger(GUIController.class.getName()).log(Level.SEVERE, null, ex1);
                        }
                    } catch (DistanceException ex) {
                        try {
                            Stage stage = new Stage();
                            Parent root = FXMLLoader.load(getClass().getResource("DistanceExceptionScreen.fxml"));
                            Scene scene = new Scene(root);
                            stage.setScene(scene);
                            stage.show();
                        } catch (IOException ex1) {
                            Logger.getLogger(GUIController.class.getName()).log(Level.SEVERE, null, ex1);
                        }
                    }
                }
            }
            updateParcelComboBoxes();
        }
    }

    @FXML
    private void deleteParcelButtonPressed(ActionEvent event) {
        GameState gs = GameState.getInstance();
        if (!createParcelComboBox.getSelectionModel().getSelectedItem().toString().equals("Luo uusi paketti")) {
            String name = createParcelComboBox.getSelectionModel().getSelectedItem().toString();
            gs.removeParcel(name);
        }
        updateParcelComboBoxes();
    }

    @FXML
    private void removeOrderButtonPressed(ActionEvent event) {
        if (ordersListView.getSelectionModel().getSelectedItem() != null) {
            GameState gs = GameState.getInstance();
            gs.removeOrder(ordersListView.getSelectionModel().getSelectedIndex());
            updateOrders();
        }
    }

    @FXML
    private void deleteSaveButtonPressed(ActionEvent event) {
        GameState gs = GameState.getInstance();
        gs.deleteSave();
        try {
            Stage stage = new Stage();
            Parent root = FXMLLoader.load(getClass().getResource("SaveDeletedScreen.fxml"));
            Scene scene = new Scene(root);
            stage.setScene(scene);
            stage.show();
        } catch (IOException ex1) {
            Logger.getLogger(GUIController.class.getName()).log(Level.SEVERE, null, ex1);
        }
        Stage stage = ((Stage) mapView.getScene().getWindow());
        stage.fireEvent(new WindowEvent(stage, WindowEvent.WINDOW_CLOSE_REQUEST));
    }

    private void updateStatistics() {
        GameState gs = GameState.getInstance();

        moneyLabel.setText(Integer.toString(gs.getMoney()));
        statsTextArea.setText("Rahaa tienattu yhteensä: " + gs.getMoneyEarned() + System.lineSeparator()
                + "Rahaa käytetty yhteensä: " + gs.getMoneySpent() + System.lineSeparator()
                + "Paketteja lähetetty yhteensä: " + gs.getParcelsSent() + System.lineSeparator()
                + "Tilauksia toimitettu yhteensä: " + gs.getOrdersDelivered() + System.lineSeparator()
                + "SmartPost automaatteja ostettu: " + gs.getSmartPostsBought() + System.lineSeparator()
                + "SmartPost automaatteja myyty: " + gs.getSmartPostsSold());
    }

    private void updateComboBoxes() {
        GameState gs = GameState.getInstance();

        buyComboBox.getItems().clear();
        sellComboBox.getItems().clear();
        sourceComboBox.getItems().clear();
        destinationComboBox.getItems().clear();
        objectComboBox.getItems().clear();

        buyComboBox.getItems().addAll(gs.getUnboughtSmartPosts());
        sellComboBox.getItems().addAll(gs.getBoughtSmartPosts());
        sourceComboBox.getItems().addAll(gs.getBoughtSmartPosts());
        destinationComboBox.getItems().addAll(gs.getBoughtSmartPosts());
        objectComboBox.getItems().addAll(gs.getUnlockedObjects());

        buyComboBox.getSelectionModel().selectFirst();
        sellComboBox.getSelectionModel().selectFirst();
        sourceComboBox.getSelectionModel().selectFirst();
        destinationComboBox.getSelectionModel().selectFirst();
        objectComboBox.getSelectionModel().selectFirst();
    }

    private void updateParcelComboBoxes() {
        GameState gs = GameState.getInstance();

        createParcelComboBox.getItems().clear();
        sendParcelComboBox.getItems().clear();

        ArrayList create = new ArrayList();
        create.add("Luo uusi paketti");
        create.addAll(gs.getParcels());

        createParcelComboBox.getItems().addAll(create);
        sendParcelComboBox.getItems().addAll(gs.getParcels());

        createParcelComboBox.getSelectionModel().selectFirst();
        sendParcelComboBox.getSelectionModel().selectFirst();
    }

    /*Adds classes to the ComboBox*/
    private void addClasses() {
        ArrayList classes = new ArrayList();
        classes.add(1);
        classes.add(2);
        classes.add(3);
        classComboBox.getItems().addAll(classes);
        classComboBox.getSelectionModel().selectFirst();
    }

    /*Returns distance of two SmartPosts*/
    private Double getDistance(String source, String destination) {
        GameState gs = GameState.getInstance();

        ArrayList pathArray = new ArrayList();
        pathArray.addAll(gs.getSmartPost(source).getGeoData());
        pathArray.addAll(gs.getSmartPost(destination).getGeoData());

        return (Double) mapView.getEngine().executeScript("document.getDistance(" + pathArray + ")");
    }

    /*Sets a worker to draw the SmartPosts when the site is loaded*/
    private void initialDrawBoughtSmartPosts() {
        mapView.getEngine().getLoadWorker().stateProperty().addListener(new ChangeListener<Worker.State>() {
            @Override
            public void changed(ObservableValue<? extends Worker.State> observable, Worker.State oldValue, Worker.State newValue) {
                if (newValue != Worker.State.SUCCEEDED) {
                    return;
                }
                GameState gs = GameState.getInstance();
                for (Object o : sellComboBox.getItems()) {
                    String name = o.toString();
                    mapView.getEngine().executeScript("document.goToLocation(" + gs.getSmartPost(name).getGeoData() + ",'" + gs.getSmartPost(name).getInfo() + "','blue')");
                }
            }
        });
    }

    /*Updates log to the ListView*/
    private void updateLog() {
        GameState gs = GameState.getInstance();
        logListView.getItems().clear();
        logListView.getItems().addAll(gs.getLog());
    }

    /*Updates orders to the ListView*/
    public static void updateOrders() {
        GameState gs = GameState.getInstance();
        gc.ordersListView.getItems().clear();
        gc.ordersListView.getItems().addAll(gs.getOrders());
    }

}
