package timotei;

/*A type of Parcel*/
public class ThirdClassParcel extends Parcel {
    
    public ThirdClassParcel(String nameInput, ShippingObject objectInput, String sourceInput, String destinationInput, int classID, double distance, int sizeLimit, int distanceLimit, int priceInput) throws SizeException, DistanceException {
        super(nameInput, objectInput, sourceInput, destinationInput, classID, distance, sizeLimit, distanceLimit, priceInput);
        if (objectInput.getSize() > sizeLimit) {
            throw new SizeException();
        }
    }
    
    @Override
    public int send() {
        if (shippingObject.getSize() < 4000) {
            shippingObject.makeBroken();
        }
        return price;
    }
    
}
