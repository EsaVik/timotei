package timotei;

/*A class for holding order information*/
public class Order {

    private String source;
    private String destination;
    private String object;
    int sourceID;
    int destinationID;
    int objectID;

    public Order(String source, String destination, String object, int sourceID, int destinationID, int objectID) {
        this.source = source;
        this.destination = destination;
        this.object = object;
        this.sourceID = sourceID;
        this.destinationID = destinationID;
        this.objectID = objectID;
    }

    /*Returns order as String of GUI*/
    public String getOrder() {
        return object + "; " + source + " -> " + destination;
    }

    public String getSource() {
        return source;
    }

    public String getDestination() {
        return destination;
    }

    public String getObject() {
        return object;
    }

    public int getSourceID() {
        return sourceID;
    }

    public int getDestinationID() {
        return destinationID;
    }

    public int getObjectID() {
        return objectID;
    }

}
