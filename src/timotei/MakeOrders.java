package timotei;

import java.util.Random;
import java.util.TimerTask;
import javafx.application.Platform;

/*A task for the timer to make orders for the player.*/
public class MakeOrders extends TimerTask {

    @Override
    public void run() {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                GameState gs = GameState.getInstance();
                if (!(gs.getBoughtSmartPosts().size() <= 1)) {
                    Random rand = new Random();
                    int source = rand.nextInt(gs.getBoughtSmartPosts().size());
                    int destination = rand.nextInt(gs.getBoughtSmartPosts().size());
                    while (destination == source) {
                        destination = rand.nextInt(gs.getBoughtSmartPosts().size()); /*Makes sure the source and destination aren't the same*/
                    }
                    int object = rand.nextInt(gs.getUnlockedObjects().size()) + 1;
                    gs.addOrder(source, destination, object);
                    GUIController.updateOrders();
                }
            }
        });
    }

}
