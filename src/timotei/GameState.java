package timotei;

import java.util.ArrayList;
import java.util.LinkedHashMap;

/*Class for handling player data and controlling other classes*/
public class GameState {

    private static GameState instance;
    private int playerID;
    private int money;
    private int moneyEarned;
    private int moneySpent;
    private int smartPostsBought;
    private int smartPostsSold;
    private int parcelsSent;
    private int ordersDelivered;
    private LinkedHashMap<String, SmartPost> boughtSmartPosts;
    private LinkedHashMap<String, SmartPost> unboughtSmartPosts;
    private Storage storage;
    private ArrayList<Order> orders;
    private ArrayList<String> log;

    private GameState() {
    }

    /*Initializes instance, takes in playerName*/
    public static void initialize(String playerName) {
        if (instance == null) {
            instance = new GameState();
            instance.initializeStatistics(playerName);
            instance.storage = Storage.getInstance();
            instance.initializeStorage();
        }
    }

    private void initializeStatistics(String playerName) {
        ArrayList<Integer> statistics = DBMS.getInstance().getStatistics(playerName);
        playerID = statistics.get(0);
        money = statistics.get(1);
        moneyEarned = statistics.get(2);
        moneySpent = statistics.get(3);
        smartPostsBought = statistics.get(4);
        smartPostsSold = statistics.get(5);
        parcelsSent = statistics.get(6);
        ordersDelivered = statistics.get(7);

        boughtSmartPosts = DataLoader.getInstance().getBoughtSmartPosts(playerID);
        unboughtSmartPosts = DataLoader.getInstance().getUnboughtSmartPosts(playerID);
        orders = DataLoader.getInstance().getOrders(playerID);
        log = DataLoader.getInstance().getLog(playerID);
    }

    private void initializeStorage() {
        try {
            ArrayList<Integer> class1Data = DBMS.getInstance().getClassData(1);
            ArrayList<Integer> class2Data = DBMS.getInstance().getClassData(2);
            ArrayList<Integer> class3Data = DBMS.getInstance().getClassData(3);
            for (ArrayList<String> data : DBMS.getInstance().getParcels(playerID)) {
                switch (Integer.parseInt(data.get(2))) {
                    case 1:
                        storage.addParcel(1, data.get(0), data.get(3), data.get(4), storage.objectSelection(Integer.parseInt(data.get(1))), 0.0, class1Data.get(1), class1Data.get(2), class1Data.get(0));
                        break;
                    case 2:
                        storage.addParcel(2, data.get(0), data.get(3), data.get(4), storage.objectSelection(Integer.parseInt(data.get(1))), 0.0, class2Data.get(1), class2Data.get(2), class2Data.get(0));
                        break;
                    case 3:
                        storage.addParcel(3, data.get(0), data.get(3), data.get(4), storage.objectSelection(Integer.parseInt(data.get(1))), 0.0, class3Data.get(1), class3Data.get(2), class3Data.get(0));
                        break;
                }
            }
        } catch (SizeException | DistanceException ex) {
            System.err.println("Pakettien lataamisessa tietokannasta tapahtui virhe metodissa Storage.initializeStorage()");
            System.err.println("SQLite3:lla on lisätty virheellinen paketti");
            System.exit(0);
        }
    }

    public void addParcel(String name, String object, String source, String destination, int classID, Double distance) throws ParcelNameUsedException, SizeException, DistanceException {
        if (storage.getParcels().contains(name)) {
            throw new ParcelNameUsedException();
        } else {
            ArrayList<Integer> data = DBMS.getInstance().getClassData(classID);
            storage.addParcel(classID, name, source, destination, objectSelection(object), distance, data.get(1), data.get(2), data.get(0));
            DBMS.getInstance().addParcel(playerID, source, destination, object, classID, name);
        }
    }

    public void updateParcel(String name, String newName, String object, String source, String destination, int classID, Double distance) throws ParcelNameUsedException, SizeException, DistanceException {
        if (storage.getParcels().contains(newName) && !name.equals(newName)) {
            throw new ParcelNameUsedException();
        } else {
            storage.removeParcel(name);
            DBMS.getInstance().removeParcel(playerID, name);
            ArrayList<Integer> data = DBMS.getInstance().getClassData(classID);
            storage.addParcel(classID, name, source, destination, objectSelection(object), distance, data.get(1), data.get(2), data.get(0));
            DBMS.getInstance().addParcel(playerID, source, destination, object, classID, newName);
        }
    }

    public void removeParcel(String name) {
        storage.removeParcel(name);
        DBMS.getInstance().removeParcel(playerID, name);
    }

    /*Takes in object name, returns a ShippingObject of the type*/
    private ShippingObject objectSelection(String name) {
        if (name.equals("Tiili")) {
            return new Tiili();
        } else if (name.equals("Kukkaruukku")) {
            return new Kukkaruukku();
        } else if (name.equals("Jalkapallo")) {
            return new Jalkapallo();
        } else if (name.equals("Palapeli")) {
            return new Palapeli();
        } else {
            return null;
        }
    }

    public static GameState getInstance() {
        return instance;
    }

    public ArrayList getUnboughtSmartPosts() {
        ArrayList keys = new ArrayList();
        for (String key : unboughtSmartPosts.keySet()) {
            keys.add(key);
        }
        return keys;
    }

    public ArrayList getBoughtSmartPosts() {
        ArrayList keys = new ArrayList();
        for (String key : boughtSmartPosts.keySet()) {
            keys.add(key);
        }
        return keys;
    }

    public ArrayList getParcels() {
        return storage.getParcels();
    }

    public Parcel getParcel(String name) {
        return storage.getParcel(name);
    }

    public ArrayList getUnlockedObjects() {
        ArrayList objects = new ArrayList();
        objects.add("Tiili");
        if (boughtSmartPosts.size() >= 5) {
            objects.add("Kukkaruukku");
        }
        if (boughtSmartPosts.size() >= 10) {
            objects.add("Jalkapallo");
        }
        if (boughtSmartPosts.size() >= 15) {
            objects.add("Palapeli");
        }
        return objects;
    }

    public int getPlayerID() {
        return playerID;
    }

    public int getSmartPostsSold() {
        return smartPostsSold;
    }

    public int getSmartPostsBought() {
        return smartPostsBought;
    }

    public int getParcelsSent() {
        return parcelsSent;
    }

    public int getOrdersDelivered() {
        return ordersDelivered;
    }

    public int getMoneySpent() {
        return moneySpent;
    }

    public int getMoneyEarned() {
        return moneyEarned;
    }

    public int getMoney() {
        return money;
    }

    public SmartPost getSmartPost(String name) {
        if (boughtSmartPosts.containsKey(name)) {
            return boughtSmartPosts.get(name);
        } else {
            return unboughtSmartPosts.get(name);
        }
    }

    public Storage getStorage() {
        return storage;
    }

    public ArrayList getLog() {
        return log;
    }

    /*Buys a SmartPost and processes money used*/
    public void buySmartPost(String name) throws MoneyException {
        if (money < 100) {
            throw new MoneyException();
        } else {
            moneySpent += 100;
            money -= 100;
            smartPostsBought++;
            DBMS.getInstance().buySmartPost(playerID, unboughtSmartPosts.get(name).getID());
            boughtSmartPosts.put(name, unboughtSmartPosts.get(name));
            unboughtSmartPosts.remove(name);
            pushStats();
        }
    }

    /*Sells a SmartPost and processes money used*/
    public void sellSmartPost(String name) {
        money += 75;
        moneyEarned += 75;
        smartPostsSold++;
        DBMS.getInstance().sellSmartPost(playerID, boughtSmartPosts.get(name).getID());
        unboughtSmartPosts.put(name, boughtSmartPosts.get(name));
        boughtSmartPosts.remove(name);
        pushStats();
    }

    /*Writes statistics to the database*/
    public void pushStats() {
        DBMS.getInstance().updateStatistics(playerID, money, moneyEarned, moneySpent, smartPostsBought, smartPostsSold);
    }

    /*Sends a parcel and processes money used*/
    public void sendParcel(String name) throws MoneyException {
        Parcel parcel = getParcel(name);
        if (money < parcel.getPrice()) {
            throw new MoneyException();
        } else {
            writeLog(name);
            DBMS.getInstance().sendParcel(playerID, name);
            money -= parcel.getPrice();
            moneySpent += parcel.send();
            if (parcel.getObject().isBroken()) {
                money -= 50;
                moneySpent += 50;
            }
            parcelsSent++;
            checkOrders(parcel.getSource(), parcel.getDestination(), parcel.getObject().getName());
            removeParcel(name);
            pushStats();
        }
    }

    /*Checks if a sent parcel delivers an order*/
    public void checkOrders(String source, String destination, String object) {
        for (Order order : orders) {
            if (order.getSource().equals(source) && order.getDestination().equals(destination) && order.getObject().equals(object)) {
                money += 100;
                moneyEarned += 100;
                ordersDelivered++;
                DBMS.getInstance().deliverOrder(playerID, order.getSourceID(), order.getDestinationID(), order.getObjectID());
                orders.remove(order);
                return;
            }
        }
    }

    public ArrayList getOrders() {
        ArrayList orderList = new ArrayList();
        for (Order order : orders) {
            orderList.add(order.getOrder());
        }
        return orderList;
    }

    public void removeOrder(int index) {
        Order order = orders.get(index);
        DBMS.getInstance().deleteOrder(playerID, order.getSourceID(), order.getDestinationID(), order.getObjectID());
        orders.remove(index);
    }

    public void deleteSave() {
        DBMS.getInstance().deleteSave(playerID);
    }

    public void writeLog(String name) {
        DBMS.getInstance().addLog(name, playerID);
        log = DataLoader.getInstance().getLog(playerID);
    }

    public void addOrder(int source, int destination, int objectID) {
        String sourceName = boughtSmartPosts.keySet().toArray()[source].toString();
        int sourceID = boughtSmartPosts.get(boughtSmartPosts.keySet().toArray()[source]).getID();
        String destinationName = boughtSmartPosts.keySet().toArray()[destination].toString();
        int destinationID = boughtSmartPosts.get(boughtSmartPosts.keySet().toArray()[destination]).getID();
        String objectName = storage.objectSelection(objectID).getName();

        for (Order order : orders) {
            if ((order.sourceID == sourceID) && (order.destinationID == destinationID) && (order.objectID == objectID)) {
                return;
            }
        }
        DBMS.getInstance().addOrder(playerID, sourceID, destinationID, objectID);
        orders.add(new Order(sourceName, destinationName, objectName, sourceID, destinationID, objectID));
    }

}
