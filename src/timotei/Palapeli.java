package timotei;

/*A type of ShippingObject*/
public class Palapeli extends ShippingObject {
    
    public Palapeli() {
        breakable = true;
        broken = false;
        size = 4000;
    }
    
}
