package timotei;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.stage.Stage;

/*Controller for info screens with only a close button*/
public class ScreenController implements Initializable {

    @FXML
    private Button closeButton;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
    }

    /*Closes the screen*/
    @FXML
    private void closeButtonPressed(ActionEvent event) {
        ((Stage) closeButton.getScene().getWindow()).close();
    }

}
