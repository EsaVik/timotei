package timotei;

import java.sql.*;
import java.util.ArrayList;

/*Class that handles database IO in SQL*/
public class DBMS {

    private static DBMS instance = null;

    public static DBMS getInstance() {
        if (instance == null) {
            instance = new DBMS();
        }
        return instance;
    }

    private DBMS() {
    }

    /*Adds data into SmartPosts, Locations and PostalCodes tables*/
    public void addSmartPost(String name, String availability, String address, Double latitude, Double longitude, String postalCode, String city) {
        Connection c = null;
        PreparedStatement pstmt = null;
        int tmpCount;
        int tmpID;

        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:database.db");

            pstmt = c.prepareStatement("SELECT COUNT(*) FROM PostalCodes WHERE PostalCode = ?");
            pstmt.setString(1, postalCode);
            tmpCount = pstmt.executeQuery().getInt(1);

            if (tmpCount == 0) {
                pstmt = c.prepareStatement("INSERT INTO PostalCodes(PostalCode, City) VALUES(?, ?)");
                pstmt.setString(1, postalCode);
                pstmt.setString(2, city);
                pstmt.executeUpdate();
            }

            pstmt = c.prepareStatement("SELECT COUNT(*) FROM Locations WHERE Latitude = ? AND Longitude = ?");
            pstmt.setDouble(1, latitude);
            pstmt.setDouble(2, longitude);
            tmpCount = pstmt.executeQuery().getInt(1);

            if (tmpCount == 0) {
                pstmt = c.prepareStatement("SELECT PostalCodeID FROM PostalCodes WHERE PostalCode = ?");
                pstmt.setString(1, postalCode);
                tmpID = pstmt.executeQuery().getInt(1);

                pstmt = c.prepareStatement("INSERT INTO Locations(PostalCodeID, Address, Latitude, Longitude) VALUES(?, ?, ?, ?)");
                pstmt.setInt(1, tmpID);
                pstmt.setString(2, address);
                pstmt.setDouble(3, latitude);
                pstmt.setDouble(4, longitude);
                pstmt.executeUpdate();
            }

            pstmt = c.prepareStatement("SELECT LocationID FROM Locations WHERE Latitude = ? AND Longitude = ?");
            pstmt.setDouble(1, latitude);
            pstmt.setDouble(2, longitude);
            tmpID = pstmt.executeQuery().getInt(1);

            pstmt = c.prepareStatement("SELECT COUNT(*) FROM SmartPosts WHERE LocationID = ?");
            pstmt.setInt(1, tmpID);
            tmpCount = pstmt.executeQuery().getInt(1);

            if (tmpCount == 0) {
                pstmt = c.prepareStatement("INSERT INTO SmartPosts(LocationID, Name, Availability) VALUES(?, ?, ?)");
                pstmt.setInt(1, tmpID);
                pstmt.setString(2, name);
                pstmt.setString(3, availability);
                pstmt.executeUpdate();
            }

            c.close();
        } catch (ClassNotFoundException | SQLException e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        }
    }

    /*Adds initial player data into the database, returns playerID*/
    public int addPlayer(String name, int money) {
        Connection c = null;
        PreparedStatement pstmt = null;
        int playerID = -1;
        int tmpCount;

        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:database.db");

            pstmt = c.prepareStatement("SELECT COUNT(*) FROM Players WHERE Name = ?");
            pstmt.setString(1, name);
            tmpCount = pstmt.executeQuery().getInt(1);

            if (tmpCount == 0) {
                pstmt = c.prepareStatement("INSERT INTO Players(Name) VALUES(?)");
                pstmt.setString(1, name);
                pstmt.executeUpdate();

                pstmt = c.prepareStatement("SELECT PlayerID FROM Players WHERE Name = ?");
                pstmt.setString(1, name);
                playerID = pstmt.executeQuery().getInt(1);
            }

            pstmt = c.prepareStatement("SELECT COUNT(*) FROM Statistics WHERE PlayerID = ?");
            pstmt.setInt(1, playerID);
            tmpCount = pstmt.executeQuery().getInt(1);

            if (tmpCount == 0) {
                pstmt = c.prepareStatement("INSERT INTO Statistics(PlayerID, Money) VALUES(?, ?)");
                pstmt.setInt(1, playerID);
                pstmt.setInt(2, money);
                pstmt.executeUpdate();
            }

            c.close();
        } catch (ClassNotFoundException | SQLException e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        }
        return playerID;
    }

    /*Buys a SmartPost in the database*/
    public void buySmartPost(int playerID, int smartPostID) {
        Connection c = null;
        PreparedStatement pstmt = null;
        int playerCount;
        int smartPostCount;
        int tmpCount;

        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:database.db");

            pstmt = c.prepareStatement("SELECT COUNT(*) FROM Players WHERE PlayerID = ?");
            pstmt.setInt(1, playerID);
            playerCount = pstmt.executeQuery().getInt(1);

            pstmt = c.prepareStatement("SELECT COUNT(*) FROM SmartPosts WHERE SmartPostID = ?");
            pstmt.setInt(1, smartPostID);
            smartPostCount = pstmt.executeQuery().getInt(1);

            pstmt = c.prepareStatement("SELECT COUNT(*) FROM BoughtSmartPosts WHERE PlayerID = ? AND SmartPostID = ?");
            pstmt.setInt(1, playerID);
            pstmt.setInt(2, smartPostID);
            tmpCount = pstmt.executeQuery().getInt(1);

            if ((playerCount == 1) && (smartPostCount == 1) && (tmpCount == 0)) {
                pstmt = c.prepareStatement("INSERT INTO BoughtSmartPosts(PlayerID, SmartPostID) VALUES(?, ?)");
                pstmt.setInt(1, playerID);
                pstmt.setInt(2, smartPostID);
                pstmt.executeUpdate();
            }

            c.close();
        } catch (ClassNotFoundException | SQLException e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        }
    }

    /*Sells a SmartPost in the database*/
    public void sellSmartPost(int playerID, int smartPostID) {
        Connection c = null;
        PreparedStatement pstmt = null;
        int playerCount;
        int smartPostCount;
        int tmpCount;

        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:database.db");

            pstmt = c.prepareStatement("SELECT COUNT(*) FROM Players WHERE PlayerID = ?");
            pstmt.setInt(1, playerID);
            playerCount = pstmt.executeQuery().getInt(1);

            pstmt = c.prepareStatement("SELECT COUNT(*) FROM SmartPosts WHERE SmartPostID = ?");
            pstmt.setInt(1, smartPostID);
            smartPostCount = pstmt.executeQuery().getInt(1);

            pstmt = c.prepareStatement("SELECT COUNT(*) FROM BoughtSmartPosts WHERE PlayerID = ? AND SmartPostID = ?");
            pstmt.setInt(1, playerID);
            pstmt.setInt(2, smartPostID);
            tmpCount = pstmt.executeQuery().getInt(1);

            if ((playerCount == 1) && (smartPostCount == 1) && (tmpCount == 1)) {
                pstmt = c.prepareStatement("DELETE FROM BoughtSmartPosts WHERE PlayerID = ? AND SmartPostID = ?");
                pstmt.setInt(1, playerID);
                pstmt.setInt(2, smartPostID);
                pstmt.executeUpdate();
            }

            c.close();
        } catch (ClassNotFoundException | SQLException e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        }
    }

    /*Returns class information in an ArrayList*/
    public ArrayList<Integer> getClassData(int classID) {
        ArrayList<Integer> data = new ArrayList();
        ResultSet results;
        Connection c = null;
        PreparedStatement pstmt = null;

        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:database.db");

            pstmt = c.prepareStatement("SELECT Price, SizeLimit, DistanceLimit FROM Classes WHERE ClassID = ?");
            pstmt.setInt(1, classID);
            results = pstmt.executeQuery();
            
            data.add(results.getInt(1));
            data.add(results.getInt(2));
            data.add(results.getInt(3));
            
            c.close();
        } catch (ClassNotFoundException | SQLException e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        }

        return data;
    }
    
    /*Initializes classes in the Classes table*/
    public void initializeClasses() {
        Connection c = null;
        PreparedStatement pstmt = null;

        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:database.db");

            pstmt = c.prepareStatement("SELECT COUNT(*) FROM Classes WHERE ClassID = 1");
            if (pstmt.executeQuery().getInt(1) == 0) {
                pstmt = c.prepareStatement("INSERT INTO Classes Values(1, 40, 3000, 150)");
                pstmt.executeUpdate();
            }

            pstmt = c.prepareStatement("SELECT COUNT(*) FROM Classes WHERE ClassID = 2");
            if (pstmt.executeQuery().getInt(1) == 0) {
                pstmt = c.prepareStatement("INSERT INTO Classes Values(2, 90, 2000, 0)");
                pstmt.executeUpdate();
            }

            pstmt = c.prepareStatement("SELECT COUNT(*) FROM Classes WHERE ClassID = 3");
            if (pstmt.executeQuery().getInt(1) == 0) {
                pstmt = c.prepareStatement("INSERT INTO Classes Values(3, 70, 5000, 0)");
                pstmt.executeUpdate();
            }

            c.close();
        } catch (ClassNotFoundException | SQLException e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        }
    }

    public void addParcel(int playerID, String sourceName, String destinationName, String objectName, int classID, String name) {
        Connection c = null;
        PreparedStatement pstmt = null;
        int tmpCount;
        int sourceID;
        int destinationID;
        int objectID;

        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:database.db");

            pstmt = c.prepareStatement("SELECT COUNT(*) FROM Parcels WHERE PlayerID = ? AND Name = ? AND Sent = ?");
            pstmt.setInt(1, playerID);
            pstmt.setString(2, name);
            pstmt.setBoolean(3, false);
            tmpCount = pstmt.executeQuery().getInt(1);

            pstmt = c.prepareStatement("SELECT SmartPostID FROM SmartPosts WHERE Name = ?");
            pstmt.setString(1, sourceName);
            sourceID = pstmt.executeQuery().getInt(1);

            pstmt = c.prepareStatement("SELECT SmartPostID FROM SmartPosts WHERE Name = ?");
            pstmt.setString(1, destinationName);
            destinationID = pstmt.executeQuery().getInt(1);

            pstmt = c.prepareStatement("SELECT ObjectID FROM Objects WHERE Name = ?");
            pstmt.setString(1, objectName);
            objectID = pstmt.executeQuery().getInt(1);

            if ((tmpCount == 0) && (sourceID != 0) && (destinationID != 0)) {
                pstmt = c.prepareStatement("INSERT INTO Parcels(PlayerID, SourceID, DestinationID, ObjectID, ClassID, Name, Sent) VALUES(?, ?, ?, ?, ?, ?, ?)");
                pstmt.setInt(1, playerID);
                pstmt.setInt(2, sourceID);
                pstmt.setInt(3, destinationID);
                pstmt.setInt(4, objectID);
                pstmt.setInt(5, classID);
                pstmt.setString(6, name);
                pstmt.setBoolean(7, false);
                pstmt.executeUpdate();
            }

            c.close();
        } catch (ClassNotFoundException | SQLException e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        }
    }

    public ArrayList<ArrayList<String>> getBoughtSmartPosts(int playerID) {
        ArrayList<ArrayList<String>> smartPosts = new ArrayList();
        Connection c = null;
        ResultSet results;
        PreparedStatement pstmt = null;

        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:database.db");

            pstmt = c.prepareStatement("SELECT SmartPosts.Name, Smartposts.Availability, Locations.Address, Locations.Latitude, Locations.Longitude, PostalCodes.PostalCode, PostalCodes.City, SmartPosts.SmartPostID "
                    + "FROM (SmartPosts INNER JOIN Locations ON SmartPosts.LocationID = Locations.LocationID) "
                    + "INNER JOIN PostalCodes ON Locations.PostalCodeID = PostalCodes.PostalCodeID "
                    + "WHERE EXISTS (SELECT * FROM BoughtSmartPosts WHERE SmartPosts.SmartPostID = BoughtSmartPosts.SmartPostID AND BoughtSmartPosts.PlayerID = ?)");
            pstmt.setInt(1, playerID);
            results = pstmt.executeQuery();

            while (results.next()) {
                ArrayList<String> smartPost = new ArrayList();
                smartPost.add(results.getString(1));
                smartPost.add(results.getString(2));
                smartPost.add(results.getString(3));
                smartPost.add(results.getString(4));
                smartPost.add(results.getString(5));
                smartPost.add(results.getString(6));
                smartPost.add(results.getString(7));
                smartPost.add(results.getString(8));

                smartPosts.add(smartPost);
            }

            c.close();
        } catch (ClassNotFoundException | SQLException e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        }

        return smartPosts;
    }

    public ArrayList<ArrayList<String>> getUnboughtSmartPosts(int playerID) {
        ArrayList<ArrayList<String>> smartPosts = new ArrayList();
        Connection c = null;
        ResultSet results;
        PreparedStatement pstmt = null;

        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:database.db");

            pstmt = c.prepareStatement("SELECT SmartPosts.Name, Smartposts.Availability, Locations.Address, Locations.Latitude, Locations.Longitude, PostalCodes.PostalCode, PostalCodes.City, SmartPosts.SmartPostID "
                    + "FROM (SmartPosts INNER JOIN Locations ON SmartPosts.LocationID = Locations.LocationID) "
                    + "INNER JOIN PostalCodes ON Locations.PostalCodeID = PostalCodes.PostalCodeID "
                    + "WHERE NOT EXISTS (SELECT * FROM BoughtSmartPosts WHERE SmartPosts.SmartPostID = BoughtSmartPosts.SmartPostID AND BoughtSmartPosts.PlayerID = ?)");
            pstmt.setInt(1, playerID);
            results = pstmt.executeQuery();

            while (results.next()) {
                ArrayList<String> smartPost = new ArrayList();
                smartPost.add(results.getString(1));
                smartPost.add(results.getString(2));
                smartPost.add(results.getString(3));
                smartPost.add(results.getString(4));
                smartPost.add(results.getString(5));
                smartPost.add(results.getString(6));
                smartPost.add(results.getString(7));
                smartPost.add(results.getString(8));

                smartPosts.add(smartPost);
            }

            c.close();
        } catch (ClassNotFoundException | SQLException e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        }

        return smartPosts;
    }

    public ArrayList<Integer> getStatistics(String playerName) {
        ArrayList<Integer> statistics = new ArrayList();

        Connection c = null;
        PreparedStatement pstmt = null;
        int tmpCount;
        ResultSet results;

        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:database.db");

            pstmt = c.prepareStatement("SELECT COUNT(*) FROM Players WHERE Name = ?");
            pstmt.setString(1, playerName);
            tmpCount = pstmt.executeQuery().getInt(1);

            if (tmpCount == 1) {
                pstmt = c.prepareStatement("SELECT Players.PlayerID, Statistics.Money, Statistics.MoneyEarned, Statistics.MoneySpent, Statistics.SmartPostsBought, Statistics.SmartpostsSold "
                        + "FROM Players INNER JOIN Statistics ON Players.PlayerID = Statistics.PlayerID WHERE Players.Name = ?");
                pstmt.setString(1, playerName);
                results = pstmt.executeQuery();

                statistics.add(results.getInt(1));
                statistics.add(results.getInt(2));
                statistics.add(results.getInt(3));
                statistics.add(results.getInt(4));
                statistics.add(results.getInt(5));
                statistics.add(results.getInt(6));

                pstmt = c.prepareStatement("SELECT COUNT(*) FROM Parcels WHERE PlayerID = ? AND Sent = ?");
                pstmt.setInt(1, statistics.get(0));
                pstmt.setBoolean(2, true);
                statistics.add(pstmt.executeQuery().getInt(1));

                pstmt = c.prepareStatement("SELECT COUNT(*) FROM Orders WHERE PlayerID = ? AND Delivered = ?");
                pstmt.setInt(1, statistics.get(0));
                pstmt.setBoolean(2, true);
                statistics.add(pstmt.executeQuery().getInt(1));
            }

            c.close();
        } catch (ClassNotFoundException | SQLException e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        }

        return statistics;
    }

    public ArrayList<ArrayList<String>> getParcels(int playerID) {
        ArrayList<ArrayList<String>> parcels = new ArrayList();
        Connection c = null;
        ResultSet results;
        PreparedStatement pstmt = null;
        int sourceID;
        int destinationID;

        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:database.db");

            pstmt = c.prepareStatement("SELECT Name, ObjectID, ClassID, SourceID, DestinationID FROM Parcels WHERE PlayerID = ? AND Sent = ?");
            pstmt.setInt(1, playerID);
            pstmt.setBoolean(2, false);
            results = pstmt.executeQuery();

            while (results.next()) {
                ArrayList<String> parcel = new ArrayList();
                parcel.add(results.getString(1));
                parcel.add(results.getString(2));
                parcel.add(results.getString(3));
                sourceID = results.getInt(4);
                destinationID = results.getInt(5);

                pstmt = c.prepareStatement("SELECT Name FROM SmartPosts WHERE SmartPostID = ?");
                pstmt.setInt(1, sourceID);
                parcel.add(pstmt.executeQuery().getString(1));

                pstmt = c.prepareStatement("SELECT Name FROM SmartPosts WHERE SmartPostID = ?");
                pstmt.setInt(1, destinationID);
                parcel.add(pstmt.executeQuery().getString(1));

                parcels.add(parcel);
            }

            c.close();
        } catch (ClassNotFoundException | SQLException e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        }

        return parcels;
    }

    /*Initializes objects in the Objects table*/
    public void initializeObjects() {
        Connection c = null;
        PreparedStatement pstmt = null;

        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:database.db");

            pstmt = c.prepareStatement("SELECT COUNT(*) FROM Objects WHERE ObjectID = 1");
            if (pstmt.executeQuery().getInt(1) == 0) {
                pstmt = c.prepareStatement("INSERT INTO Objects Values(1, 'Tiili')");
                pstmt.executeUpdate();
            }

            pstmt = c.prepareStatement("SELECT COUNT(*) FROM Objects WHERE ObjectID = 2");
            if (pstmt.executeQuery().getInt(1) == 0) {
                pstmt = c.prepareStatement("INSERT INTO Objects Values(2, 'Kukkaruukku')");
                pstmt.executeUpdate();
            }

            pstmt = c.prepareStatement("SELECT COUNT(*) FROM Objects WHERE ObjectID = 3");
            if (pstmt.executeQuery().getInt(1) == 0) {
                pstmt = c.prepareStatement("INSERT INTO Objects Values(3, 'Jalkapallo')");
                pstmt.executeUpdate();
            }

            pstmt = c.prepareStatement("SELECT COUNT(*) FROM Objects WHERE ObjectID = 4");
            if (pstmt.executeQuery().getInt(1) == 0) {
                pstmt = c.prepareStatement("INSERT INTO Objects Values(4, 'Palapeli')");
                pstmt.executeUpdate();
            }

            c.close();
        } catch (ClassNotFoundException | SQLException e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        }
    }

    public void removeParcel(int playerID, String name) {
        Connection c = null;
        PreparedStatement pstmt = null;

        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:database.db");

            pstmt = c.prepareStatement("DELETE FROM Parcels WHERE PlayerID = ? AND Name = ? AND Sent = ?");
            pstmt.setInt(1, playerID);
            pstmt.setString(2, name);
            pstmt.setBoolean(3, false);
            pstmt.executeUpdate();

            c.close();
        } catch (ClassNotFoundException | SQLException e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        }
    }

    /*Updates statistics in the database based on the inputs*/
    public void updateStatistics(int playerID, int money, int moneyEarned, int moneySpent, int smartPostsBought, int smartPostsSold) {
        Connection c = null;
        PreparedStatement pstmt = null;

        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:database.db");

            pstmt = c.prepareStatement("UPDATE Statistics SET Money = ?, MoneyEarned = ?, MoneySpent = ?, SmartPostsBought = ?, SmartPostsSold = ? WHERE PlayerID = ?");
            pstmt.setInt(1, money);
            pstmt.setInt(2, moneyEarned);
            pstmt.setInt(3, moneySpent);
            pstmt.setInt(4, smartPostsBought);
            pstmt.setInt(5, smartPostsSold);
            pstmt.setInt(6, playerID);
            pstmt.executeUpdate();

            c.close();
        } catch (ClassNotFoundException | SQLException e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        }
    }

    /*Changes parcel Sent value*/
    public void sendParcel(int playerID, String name) {
        Connection c = null;
        PreparedStatement pstmt = null;

        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:database.db");

            pstmt = c.prepareStatement("UPDATE Parcels SET Sent = ? WHERE PlayerID = ? AND Name = ?");
            pstmt.setBoolean(1, true);
            pstmt.setInt(2, playerID);
            pstmt.setString(3, name);
            pstmt.executeUpdate();

            c.close();
        } catch (ClassNotFoundException | SQLException e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        }
    }

    public ArrayList<ArrayList<String>> getOrders(int playerID) {
        ArrayList<ArrayList<String>> orders = new ArrayList();
        Connection c = null;
        ResultSet results;
        PreparedStatement pstmt = null;
        int sourceID;
        int destinationID;
        int objectID;

        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:database.db");

            pstmt = c.prepareStatement("SELECT SourceID, DestinationID, ObjectID FROM Orders WHERE PlayerID = ? AND Delivered = ?");
            pstmt.setInt(1, playerID);
            pstmt.setBoolean(2, false);
            results = pstmt.executeQuery();

            while (results.next()) {
                ArrayList<String> order = new ArrayList();
                sourceID = results.getInt(1);
                destinationID = results.getInt(2);
                objectID = results.getInt(3);

                pstmt = c.prepareStatement("SELECT Name FROM SmartPosts WHERE SmartPostID = ?");
                pstmt.setInt(1, sourceID);
                order.add(pstmt.executeQuery().getString(1));

                pstmt = c.prepareStatement("SELECT Name FROM SmartPosts WHERE SmartPostID = ?");
                pstmt.setInt(1, destinationID);
                order.add(pstmt.executeQuery().getString(1));

                pstmt = c.prepareStatement("SELECT Name FROM Objects WHERE ObjectID = ?");
                pstmt.setInt(1, objectID);
                order.add(pstmt.executeQuery().getString(1));

                order.add(Integer.toString(sourceID));
                order.add(Integer.toString(destinationID));
                order.add(Integer.toString(objectID));

                orders.add(order);
            }

            c.close();
        } catch (ClassNotFoundException | SQLException e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        }

        return orders;
    }

    /*Changes order Delivered value*/
    public void deliverOrder(int playerID, int sourceID, int destinationID, int objectID) {
        Connection c = null;
        PreparedStatement pstmt = null;

        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:database.db");

            pstmt = c.prepareStatement("UPDATE Orders SET Delivered = ? WHERE PlayerID = ? AND SourceID = ? AND DestinationID = ? AND ObjectID = ?");
            pstmt.setBoolean(1, true);
            pstmt.setInt(2, playerID);
            pstmt.setInt(3, sourceID);
            pstmt.setInt(4, destinationID);
            pstmt.setInt(5, objectID);
            pstmt.executeUpdate();

            c.close();
        } catch (ClassNotFoundException | SQLException e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        }
    }

    /*Deletes data assossiated with a PlayerID*/
    public void deleteSave(int playerID) {
        Connection c = null;
        PreparedStatement pstmt = null;

        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:database.db");

            pstmt = c.prepareStatement("DELETE FROM Logs WHERE EXISTS (SELECT * FROM Parcels INNER JOIN Logs ON Parcels.ParcelID = Logs.ParcelID WHERE PlayerID = ?)");
            pstmt.setInt(1, playerID);
            pstmt.executeUpdate();

            pstmt = c.prepareStatement("DELETE FROM Parcels WHERE PlayerID = ?");
            pstmt.setInt(1, playerID);
            pstmt.executeUpdate();

            pstmt = c.prepareStatement("DELETE FROM BoughtSmartPosts WHERE PlayerID = ?");
            pstmt.setInt(1, playerID);
            pstmt.executeUpdate();

            pstmt = c.prepareStatement("DELETE FROM Orders WHERE PlayerID = ?");
            pstmt.setInt(1, playerID);
            pstmt.executeUpdate();

            pstmt = c.prepareStatement("DELETE FROM Statistics WHERE PlayerID = ?");
            pstmt.setInt(1, playerID);
            pstmt.executeUpdate();

            pstmt = c.prepareStatement("DELETE FROM Players WHERE PlayerID = ?");
            pstmt.setInt(1, playerID);
            pstmt.executeUpdate();

            c.close();
        } catch (ClassNotFoundException | SQLException e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        }
    }

    public ArrayList<String> getLog(int playerID) {
        ArrayList<String> log = new ArrayList();
        Connection c = null;
        ResultSet results;
        PreparedStatement pstmt = null;
        int sourceID;
        int destinationID;
        String source;
        String destination;
        String name;
        String timestamp;
        String entry;

        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:database.db");

            pstmt = c.prepareStatement("SELECT SourceID, DestinationID, Name, Timestamp FROM LogData WHERE PlayerID = ?");
            pstmt.setInt(1, playerID);
            results = pstmt.executeQuery();

            while (results.next()) {
                sourceID = results.getInt(1);
                destinationID = results.getInt(2);
                name = results.getString(3);
                timestamp = results.getString(4);

                pstmt = c.prepareStatement("SELECT Name FROM SmartPosts WHERE SmartPostID = ?");
                pstmt.setInt(1, sourceID);
                source = pstmt.executeQuery().getString(1);

                pstmt = c.prepareStatement("SELECT Name FROM SmartPosts WHERE SmartPostID = ?");
                pstmt.setInt(1, destinationID);
                destination = pstmt.executeQuery().getString(1);

                entry = timestamp + ": " + name + ", " + source + " -> " + destination;

                log.add(entry);
            }

            c.close();
        } catch (ClassNotFoundException | SQLException e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        }

        return log;
    }

    public void addLog(String name, int playerID) {
        Connection c = null;
        PreparedStatement pstmt = null;
        int parcelID;

        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:database.db");

            pstmt = c.prepareStatement("SELECT ParcelID FROM Parcels WHERE Name = ? AND PlayerID = ? AND Sent = ?");
            pstmt.setString(1, name);
            pstmt.setInt(2, playerID);
            pstmt.setBoolean(3, false);
            parcelID = pstmt.executeQuery().getInt(1);

            pstmt = c.prepareStatement("INSERT INTO Logs(ParcelID) VALUES(?)");
            pstmt.setInt(1, parcelID);
            pstmt.executeUpdate();

            c.close();
        } catch (ClassNotFoundException | SQLException e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        }
    }

    public void addOrder(int playerID, int sourceID, int destinationID, int objectID) {
        Connection c = null;
        PreparedStatement pstmt = null;

        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:database.db");

            pstmt = c.prepareStatement("INSERT INTO Orders(PlayerID, SourceID, DestinationID, ObjectID, Delivered) VALUES(?, ?, ?, ?, ?)");
            pstmt.setInt(1, playerID);
            pstmt.setInt(2, sourceID);
            pstmt.setInt(3, destinationID);
            pstmt.setInt(4, objectID);
            pstmt.setBoolean(5, false);
            pstmt.executeUpdate();

            c.close();
        } catch (ClassNotFoundException | SQLException e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        }
    }
    
    public void deleteOrder(int playerID, int sourceID, int destinationID, int objectID) {
        Connection c = null;
        PreparedStatement pstmt = null;

        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:database.db");

            pstmt = c.prepareStatement("DELETE FROM Orders WHERE PlayerID = ? AND SourceID = ? AND DestinationID = ? AND ObjectID = ? AND Delivered = ?");
            pstmt.setInt(1, playerID);
            pstmt.setInt(2, sourceID);
            pstmt.setInt(3, destinationID);
            pstmt.setInt(4, objectID);
            pstmt.setBoolean(5, false);
            pstmt.executeUpdate();

            c.close();
        } catch (ClassNotFoundException | SQLException e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        }
    }

}
