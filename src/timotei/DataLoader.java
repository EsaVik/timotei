package timotei;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/*Class that loads data from the database and XML*/
public class DataLoader {

    private static DataLoader instance;
    
    private DataLoader() {
    }

    public static DataLoader getInstance() {
        if (instance == null) {
            instance = new DataLoader();
        }
        return instance;
    }

    /*Updates database SmartPosts from the XML file*/
    public void updateSmartPostData() {
        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbFactory.newDocumentBuilder();
            URL url = new URL("http://smartpost.ee/fi_apt.xml");
            Document doc = db.parse(new InputSource(new InputStreamReader(url.openStream())));
            doc.getDocumentElement().normalize();
            parseSmartPostData(doc);
        } catch (ParserConfigurationException | IOException | SAXException ex) {
            System.err.println("Data loading failed: " + ex.getMessage());
            System.exit(0);
        }
    }

    /*Parses XML contents into usable format and writes them into the database*/
    private void parseSmartPostData(Document doc) {
        NodeList nodes = doc.getElementsByTagName("place");
        for (int i = 0; i < nodes.getLength(); i++) {
            Node node = nodes.item(i);
            Element e = (Element) node;
            DBMS.getInstance().addSmartPost(
                    e.getElementsByTagName("postoffice").item(0).getTextContent(),
                    e.getElementsByTagName("availability").item(0).getTextContent(),
                    e.getElementsByTagName("address").item(0).getTextContent(),
                    Double.parseDouble(e.getElementsByTagName("lat").item(0).getTextContent()),
                    Double.parseDouble(e.getElementsByTagName("lng").item(0).getTextContent()),
                    e.getElementsByTagName("code").item(0).getTextContent(),
                    e.getElementsByTagName("city").item(0).getTextContent());
        }
    }

    public LinkedHashMap<String, SmartPost> getBoughtSmartPosts(int playerID) {
        LinkedHashMap<String, SmartPost> smartPosts = new LinkedHashMap();
        
        for (ArrayList<String> data : DBMS.getInstance().getBoughtSmartPosts(playerID)) {
            smartPosts.put(data.get(0), new SmartPost(data.get(0), data.get(1), data.get(2), Double.parseDouble(data.get(3)), Double.parseDouble(data.get(4)), data.get(5), data.get(6), data.get(7)));
        }
        
        return smartPosts;
    }

    public LinkedHashMap<String, SmartPost> getUnboughtSmartPosts(int playerID) {
        LinkedHashMap<String, SmartPost> smartPosts = new LinkedHashMap();
        
        for (ArrayList<String> data : DBMS.getInstance().getUnboughtSmartPosts(playerID)) {
            smartPosts.put(data.get(0), new SmartPost(data.get(0), data.get(1), data.get(2), Double.parseDouble(data.get(3)), Double.parseDouble(data.get(4)), data.get(5), data.get(6), data.get(7)));
        }
        
        return smartPosts;
    }
    
    public ArrayList<Order> getOrders(int playerID) {
        ArrayList<Order> orders = new ArrayList();
        
        for (ArrayList<String> data : DBMS.getInstance().getOrders(playerID)) {
            orders.add(new Order(data.get(0), data.get(1), data.get(2), Integer.parseInt(data.get(3)), Integer.parseInt(data.get(4)), Integer.parseInt(data.get(5))));
        }
        
        return orders;
    }
    
    public ArrayList<String> getLog(int playerID) {
        ArrayList<String> log = DBMS.getInstance().getLog(playerID);
        return log;
    }
    
}
