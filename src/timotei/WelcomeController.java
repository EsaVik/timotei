package timotei;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.Timer;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

/*Controller for the Welcome screen*/
public class WelcomeController implements Initializable {

    @FXML
    private TextField nameTextField;
    @FXML
    private Button startButton;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
    }

    /*Initializes GameState, orderTimer and opens GUI screen*/
    @FXML
    private void onStartButtonPress(ActionEvent event) throws IOException {
        if (nameTextField.getText().trim().isEmpty()) {
            nameTextField.setText("");
            nameTextField.setPromptText("ANNA NIMESI!");
        } else {
            DataLoader.getInstance().updateSmartPostData();
            DBMS.getInstance().initializeObjects();
            DBMS.getInstance().initializeClasses();
            DBMS.getInstance().addPlayer(nameTextField.getText().trim(), 500);
            GameState.initialize(nameTextField.getText().trim());

            Stage stage = new Stage();
            Parent root = FXMLLoader.load(getClass().getResource("GUI.fxml"));
            Scene scene = new Scene(root);
            stage.setScene(scene);
            stage.show();
            Timer orders = new Timer();
            orders.scheduleAtFixedRate(new MakeOrders(), 0, 30000); /*Sets a timer to make orders for the player.*/

            stage.setOnCloseRequest(new EventHandler<WindowEvent>() {
                @Override
                public void handle(WindowEvent t) {
                    stage.close();
                    orders.cancel(); /*Closes the timer thread with the window.*/
                }
            });
            ((Stage) startButton.getScene().getWindow()).close();
        }
    }

    /*Shows the Help screen*/
    @FXML
    private void onHelpButtonPress(ActionEvent event) throws IOException {
        Stage stage = new Stage();
        Parent root = FXMLLoader.load(getClass().getResource("HelpScreen.fxml"));
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

}
